#!/bin/bash
if ! [ -d "venv" ];then
    pip install --user virtualenv
    python3 -m venv venv
fi
    source venv/bin/activate
    pip install -r requirements.txt